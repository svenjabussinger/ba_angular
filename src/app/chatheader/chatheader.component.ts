// Komponente ChatheaderComponent
import {Component} from '@angular/core';
import {LoginService} from "../shared/services/login.service";

@Component({
  selector: 'ba-chat-header',
  templateUrl: './chatheader.component.html',
  styleUrls:['./chatheader.component.css']
})

export class ChatheaderComponent{

  // Dependency Injection des Service LoginService
  constructor(private loginService: LoginService){}

  logout = function(){
    this.loginService.userLogout();
  }
}
