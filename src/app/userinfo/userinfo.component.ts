// Userinfo Komponente
import {Component, Input} from '@angular/core';

@Component({
  selector: 'ba-user-info',
  templateUrl: './userinfo.component.html',
  styleUrls: ['./userinfo.component.css']
})

export class UserinfoComponent {

  // Input Binding mit @Input() Decorator
  @Input() user;
}
