// Komponente ChatComponent
import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MessageService} from '../shared/services/message.service'
import {LoginService} from "../shared/services/login.service";
import {Router} from "@angular/router";

@Component({
    selector: 'ba-chat',
    templateUrl: './chat.component.html',
    styleUrls: ['./chat.component.css']
})

export class ChatComponent implements OnInit, AfterViewChecked{

    // Dependency Injection der Services MessageService, LoginService und Router
    constructor(private messageService: MessageService, private loginService: LoginService, private router: Router){
      this.currentUser = this.loginService.getCurrentUser();
      if(this.currentUser == null){
        router.navigate(['login']);
      }
    };

  currentUser;
  currentUserData;
  messages;
  currentUserID;

  @ViewChild('messagepanel') private messagePanel : ElementRef;

  // Lebenszyklusmethode OnInit
  ngOnInit(){
    this.messages = this.messageService.getMessages();

    this.currentUserID = this.currentUser.uid;
    this.currentUserData = this.loginService.getUserData(this.currentUser.uid);

    this.scrollToBottom();

  }

  // Lebenszyklusmethode AfterViewChecked
  ngAfterViewChecked() {
    this.scrollToBottom();
  }

  scrollToBottom(): void {
    try {
      this.messagePanel.nativeElement.scrollTop = this.messagePanel.nativeElement.scrollHeight;
    } catch(err) { }
  }


}

