// Komponente MessageinputComponent
import { Component} from '@angular/core';
import {MessageService} from "../shared/services/message.service";
import {LoginService} from "../shared/services/login.service";


@Component({
    selector: 'ba-message-input',
    templateUrl: './messageinput.component.html',
    styleUrls: ['./messageinput.component.css']
})

export class MessageinputComponent {

  // Dependency Injection des Service MessageService und LoginService
    constructor(private messageService: MessageService, private loginService: LoginService){};

  newmessage: string = '';

  sendKeyUpMessage = function ($event) {
    if($event.keyCode === 13) {
      console.log("new message: " + this.newmessage + " needs to be sended to messages");
      this.messageService.saveMessage(this.newmessage, this.loginService.getCurrentUser().uid);
      this.newmessage = '';
    }
  };

  sendNewMessage = function () {
        console.log("new message: " + this.newmessage + " needs to be sended to messages");
        this.messageService.saveMessage(this.newmessage, this.loginService.getCurrentUser().uid);
        this.newmessage = '';
  };
}
