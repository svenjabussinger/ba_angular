// Komponente LoginComponent
import {Component} from '@angular/core';
import {LoginService} from "../shared/services/login.service";

@Component({
  selector: 'ba-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent{

  // Dependency Injection des Service LoginService
  constructor(private loginService: LoginService){}

  login : any = {
    email: '',
    password: ''
  };

  loginUser = function(){
    this.loginService.userLogin(this.login.email, this.login.password);
    console.log("The User is getting logged in");
  }

}
