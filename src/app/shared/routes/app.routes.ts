// Router
import { RouterModule, Routes } from '@angular/router';
import {LoginViewComponent} from "../../views/loginView/loginView.component";
import {ChatViewComponent} from "../../views/chatView/chatView.component";
import {LogoutViewComponent} from "../../views/logoutView/logoutView.component";

// Router Definition
const appRoutes : Routes = [
  {path: 'login', component: LoginViewComponent},
  {path: 'chat', component: ChatViewComponent},
  {path: 'logout', component: LogoutViewComponent},
  {path: '', redirectTo: '/chat', pathMatch: 'full'}
];

// Bereitstellung der Routen
export const AppRouter = RouterModule.forRoot(appRoutes);
