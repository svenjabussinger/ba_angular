// Service LoginService
import {Injectable} from "@angular/core";
import { AngularFireAuth } from 'angularfire2/auth';
import {Observable} from "rxjs";
import * as firebase from "firebase/app";
import {Router} from "@angular/router";
import {AngularFireDatabase} from "angularfire2/database/database";

// Dependency Injection ermöglichen mit @Injectable() Decorator
@Injectable()
export class LoginService {

  user: Observable<firebase.User>;


  constructor(public afAuth: AngularFireAuth, public af: AngularFireDatabase, private router: Router) {
    this.user = this.afAuth.authState;

  }

  //Login User mit Firebase Authentication
  userLogin = function(email, password){
    this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then(()=> {
      console.log("The User got logged in successfully");
        // programmatisch Route zu chat wechseln
        this.router.navigate(['chat'])
    })
      .catch(function(error) {
        // Handle Errors here.
        let errorCode = error.code;
        let errorMessage = error.message;
        if (errorCode === 'auth/wrong-password') {
          alert('Wrong password.');
        } else {
          alert(errorMessage);
        }
        console.log(error);
      });
  };

  // neuen Nutzer registrieren mit Firebase Authentication
  userRegister = function(firstname, lastname, email, birthday, password){
    this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((user) => {
        console.log("A new User got registered successfully");
        this.saveUserInfoFromForm(user.uid, firstname, lastname, birthday);
        this.userLogin(email, password);
      })
      .catch((error) => {
        // Handle Errors here.
        let errorCode = error.code;
        let errorMessage = error.message;
        if (errorCode == 'auth/weak-password') {
          alert('The password is too weak.');
        } else {
          alert(errorMessage);
        }
        console.log(error);
      });
  };

  // zusätzliche Nutzerdaten bei Registrierung in der Firebase Datenbank speichern
  saveUserInfoFromForm(uid, firstname, lastname, birthday) {
    this.af.object('/userData/' + uid).set({
      firstname: firstname,
      lastname: lastname,
      birthday: birthday
    });
  }


  // Logout User mit Firebase Authentication
  userLogout() {
    this.afAuth.auth.signOut();
  }

  // Return eingeloggten User mit Firebase Authentication
  getCurrentUser(){
    return this.afAuth.auth.currentUser;
  }

  // Return Nutzerdaten für bestimmten Nutzer
  getUserData(uid){
    return this.af.object('/userData/' + uid);
  }


}
