// Service Message Service
import {Injectable} from "@angular/core";
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';

// Dependency Injection ermöglichen mit @Injectable() Decorator
@Injectable()
export class MessageService{
  public messages: FirebaseListObservable<any[]>;

  constructor(public af: AngularFireDatabase){
    // messages initialisieren mit Datenbankabfrage
    this.messages = af.list('/messages', {
      query: {
        limitToLast: 50
      }
    });
  }

  // neue Nachricht speichern in Firebase Datenbank
  saveMessage(content: string, uid: string) {
    let message = {
      content: content,
      uid: uid,
      timestamp: Date.now()
    };

    this.messages.push(message);
  }

  // Return Nachrichten
  getMessages = function(){
    return this.messages;
  }



}
