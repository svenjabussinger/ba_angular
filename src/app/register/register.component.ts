// Komponente RegisterComponent
import {Component} from '@angular/core';
import {LoginService} from "../shared/services/login.service";

@Component({
  selector: 'ba-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent{

  // Dependency Injection des Service LoginService
  constructor(private loginService: LoginService){}

  user : any = {
    firstname: '',
    lastname: '',
    email: '',
    birthday: '',
    password: ''
  };

  registerUser = function(){
    this.loginService.userRegister(this.user.firstname, this.user.lastname,this.user.email, this.user.birthday, this.user.password);
  }
}
