//Komponente LoginViewComponent
import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'ba-login-view',
    templateUrl: 'loginView.component.html',
    styleUrls: ['loginView.component.css']
})

export class LoginViewComponent {
  loginView: boolean = true;

  changeLogin =  function(){
    if(this.loginView === true){
      this.loginView = false;
    }else{
      this.loginView = true;
    }
}
}
