// Komponente LogoutViewComponent
import {Component} from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'ba-logout-view',
    templateUrl: 'logoutView.component.html',
    styleUrls: ['logoutView.component.css']
})

export class LogoutViewComponent {

}
