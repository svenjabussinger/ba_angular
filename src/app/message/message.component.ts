// Komponente MessageComponent
import {Component, Input, OnInit} from '@angular/core';
import {LoginService} from "../shared/services/login.service";

@Component({
    selector: 'ba-message',
    templateUrl: './message.component.html',
    styleUrls: ['./message.component.css']
})

export class MessageComponent implements OnInit{

  isUsersMessage: boolean;
  messageUser;

  // Dependency Injection des Service LoginService
  constructor(private loginService: LoginService){}

  //Input Binding mit @Input() Decorator
  @Input() msg;
  @Input() userID;

  checkUsersMessage = function(){
    if(this.userID === this.msg.uid){
      this.isUsersMessage = true;
    }else{
      this.isUsersMessage = false;
    }
  };

  // Implementierung der Lebenszyklusmethode OnInit
  ngOnInit(){
    this.checkUsersMessage();
    this.messageUser = this.loginService.getUserData(this.msg.uid);

  }



}
