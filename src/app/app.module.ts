//Hauptmodul AppModule
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";

import { AngularFireModule } from 'angularfire2';
import { AppComponent } from './app.component';
import {MessageComponent} from "./message/message.component";
import {ChatComponent} from "./chat/chat.component";
import {MessageinputComponent} from "./messageinput/messageinput.component";
import {MessageService} from "./shared/services/message.service";
import {AppRouter} from './shared/routes/app.routes';
import {LoginViewComponent} from "./views/loginView/loginView.component";
import {ChatViewComponent} from "./views/chatView/chatView.component";
import {LogoutViewComponent} from "./views/logoutView/logoutView.component";
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./register/register.component";
import {HttpModule} from "@angular/http";
import {AngularFireDatabaseModule} from "angularfire2/database/database.module";
import {AngularFireAuthModule} from "angularfire2/auth/auth.module";
import {LoginService} from "./shared/services/login.service";
import {UserinfoComponent} from "./userinfo/userinfo.component";
import {ChatheaderComponent} from "./chatheader/chatheader.component";

//Konfiguration für Firebase Datenbank
export const firebaseConfig = {
  apiKey: 'AIzaSyBuFdA9Vaezoqm5G5xQ4Ztq8kj05ZOmNzY',
  authDomain: 'chat-app-92b79.firebaseapp.com',
  databaseURL: 'https://chat-app-92b79.firebaseio.com',
  storageBucket: 'chat-app-92b79.appspot.com',
  messagingSenderId: '297794186257'
};

@NgModule({
  declarations: [
    AppComponent, ChatComponent, MessageComponent, MessageinputComponent, LoginComponent, RegisterComponent, UserinfoComponent,
    LoginViewComponent, ChatViewComponent, LogoutViewComponent, ChatheaderComponent
  ],
  imports: [
    BrowserModule, FormsModule, AppRouter, HttpModule, AngularFireModule.initializeApp(firebaseConfig), AngularFireAuthModule, AngularFireDatabaseModule
  ],
  providers: [MessageService, LoginService],
  bootstrap: [AppComponent]
})
export class AppModule { }
